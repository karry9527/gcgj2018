﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallonScript : MonoBehaviour
{
    public float gas = 0;
    public float float_threshold = 100f;
    private Rigidbody2D rigid;
    public Animator animator;
    public float float_force = 20f;

	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(gas >= float_threshold)
        {
            rigid.AddForce(Vector3.up * Time.deltaTime * float_force);

        }
	}

    public void reset()
    {
        gas = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ElementBall ball;
        if((ball = collision.gameObject.GetComponent<ElementBall>()) != null)
        {
            Debug.Log("Collider");
            Debug.Log("ball.element._Symbel: " + ball.element._Symbel);
            if (ball.element._Symbel == "H2")
            {
                gas += 31;
                animator.SetFloat("gas", gas);
            }
        }
    }
}
