﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharaControl : MonoBehaviour
{
	public float walkSpeed;
	public float jumpPower;
	public float eps;
	public LayerMask groundLayer;
	public Camera gameCamera;

	private Animator anim;
	private Transform self;
	private Rigidbody2D rb2d;
	private Collider2D coll;

	// Use this for initialization
	void Start ()
	{
		self = transform;
		rb2d = GetComponent<Rigidbody2D> ();
		coll = GetComponent<Collider2D>();
		anim = self.Find("Sprite").GetComponent<Animator>();
		StartCoroutine(Push());
	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey (KeyCode.D))
		{
			dir (0);
			move (1);
		}
		else if(Input.GetKey (KeyCode.A))
		{
			dir (1);
			move (-1);
		}
		else
		{
			move (0);
            var mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
            if (mousePos.x < self.position.x)
            {
                dir(0);
            }
            else
            {
                dir(1);
            }
        }

		if(Input.GetKeyDown (KeyCode.Space) && onGround())
		{
			jump ();
		}

		anim.SetFloat("y", rb2d.velocity.y);
	}

	IEnumerator Push()
	{
		while(true)
		{
			if(Random.Range(0f, 1f) < 0.8) anim.SetTrigger("push");
			yield return new WaitForSeconds(1);
		}
	}

	void jump ()
	{
		rb2d.AddForce (Vector2.up * jumpPower);
	}

	void dir (int d)
	{
        self.rotation = Quaternion.Euler(0, d*180, 0);
	}

	void move (int d)
	{
		if(frontWall()) return;
		//rb2d.velocity = Vector2.up * rb2d.velocity.y + Vector2.right * (walkSpeed * d);
		self.position += Vector3.right * (walkSpeed * d * Time.deltaTime);
		anim.SetFloat("x", Mathf.Abs(d));
	}
	
	private bool frontWall()
	{
		Vector2 et = coll.bounds.extents; et.x *= self.right.x;
		Vector2 delta = self.right * eps;
		Vector2 start = (Vector2)coll.bounds.center + et + delta;
		Vector2 end = start - Vector2.up * (et.y * 2);
		Debug.DrawLine(start, end);
		return Physics2D.Linecast(start, end, groundLayer);
	}

	bool onGround()
	{
		Vector2 et = coll.bounds.extents;
		Vector2 start = (Vector2)coll.bounds.center - et + Vector2.down * eps;
		Vector2 end = start + Vector2.right * (et.x * 2);
		Debug.DrawLine(start, end, Color.blue);
		return Physics2D.Linecast(start, end, groundLayer).collider != null;
	}
}