﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour {

    private Animator animator;
    private float time;
    private bool status;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (animator)
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName("Idle"))
            {
                int rand = Random.Range(0, 2);
                animator.SetInteger("IdleNum", rand);
            }
        }
        time += Time.deltaTime;
        if(time > 2.0f)
        {
            time = 0;
            status = !status;
            SetIdle(status);
        }
    }

    public void SetIdle(bool status)
    {
        animator.SetBool("Idle", status);
    }
}
