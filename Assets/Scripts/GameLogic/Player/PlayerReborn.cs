﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReborn : MonoBehaviour
{
	public string damageLayer;
	public Vector3 savePos;

	private int damageLayerMask;
	private Transform trans;

	// Use this for initialization
	void Start ()
	{
		trans = transform;
		damageLayerMask = LayerMask.NameToLayer(damageLayer);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		Debug.Log("I collide something?");
		if(other.gameObject.layer == damageLayerMask)
		{
			Debug.Log("it\'s hurt!");
			trans.position = savePos;
			GameObject.Find("Balloon").GetComponent<BallonScript>().reset();
		}
	}
}