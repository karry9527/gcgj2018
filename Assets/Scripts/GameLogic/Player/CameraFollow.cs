﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public Transform leader;
	public Vector3 offset;
	public float speed;
	private Transform trans;

	// Use this for initialization
	void Start ()
	{
		trans = transform;
	}

	// Update is called once per frame
	void Update ()
	{
		Vector3 newPos = Vector3.Lerp(trans.localPosition, leader.localPosition + offset, speed * Time.deltaTime);
		newPos.z = -10;
		trans.position = newPos;
	}
}