﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour {
	private void Update()
	{
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up);
		if(hit.collider!=null && hit.collider.tag=="Player")
		{
			Debug.Log("got you!");
			SceneManager.LoadScene("End");
		}
	}
}
