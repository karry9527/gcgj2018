﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ElementKeyIn))]
[RequireComponent(typeof(ElementList))]
public class GunShot : MonoBehaviour {
    private GameObjectPool elementPool;
    public GameObject ElementBall;
    public float ShotPower = 5;
    public Transform ballTrans;
    private Transform trans;
    private Vector3 direct;
    private Element element;
    public bool fire;
    private ElementList list;
    private ElementKeyIn keyin;

    public float paddingTime;
    public float gunDistance = 0.3f;
    public Camera game_camera;


    public float ShootTimer = 99999;
    

    // Use this for initialization
    void Start ()
    {
        trans = transform;
        list = GetComponent<ElementList>();
        keyin = GetComponent<ElementKeyIn>();
        elementPool = new GameObjectPool(ElementBall, 50, ballTrans, ballTrans);
	}
	
	// Update is called once per frame
	void Update ()
    {
        var mousePos = game_camera.ScreenToWorldPoint(Input.mousePosition);
        direct = mousePos - trans.position;
        Debug.DrawLine(trans.position, trans.position + direct, Color.red);

        if (Input.GetMouseButton(1))
            fire = true;
        else
            fire = false;
        if (Input.GetMouseButton(0))
        {
            Shot();
        }
        ShootTimer += Time.deltaTime;
	}

    public void SetElement(Element element)
    {
        this.element = element;
    }

    IEnumerator safeShoot(GameObject go)
    {
        go.layer = LayerMask.NameToLayer("Default");
        go.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(paddingTime);
        go.layer = LayerMask.NameToLayer("Damage");
        go.GetComponent<Collider2D>().enabled = true;
    }

    public void Shot()
    {
        GameObject go;
        GameObjectPool pool;
        if(PoolManager.GetGameObject(keyin.keys, out go, out pool))
        {
            Shootable bullet = go.GetComponent<Shootable>();
            if (ShootTimer > bullet.ShootSpeed)
            {
                bullet.Init();
                bullet.transform.position = trans.position + direct * gunDistance;
                bullet.AddForce(direct.normalized * ShotPower);
                if (go.GetComponent<Fire>() != null) StartCoroutine(safeShoot(go));
                bullet.SetDestroy(pool, 5f);

                ShootTimer = 0;
            }
            else
            {
                pool.Recycle(bullet.gameObject);
            }
        }
        else
        {
            keyin.Clear();
        }

    }

    private IEnumerator Destroy(ElementBall ball, float time, GameObjectPool pool)
    {
        float timer = 0;
        while(timer < time)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        //list.Recycle(ball);
        pool.Recycle(ball.gameObject);
    }
}