﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSave : MonoBehaviour
{
	public LayerMask playerLayer;
	public float rayLength;
	public float offset;

	private Vector3 voffset; //offset vector
	private Transform trans;

	private void Start()
	{
		trans = transform;
		voffset = Vector3.up * offset;
	}

	void Update ()
	{
		hitPlayer();
	}

	void hitPlayer()
	{
		RaycastHit2D hit = Physics2D.Raycast(trans.position, Vector2.up, rayLength, playerLayer);
		if(hit.collider && hit.collider.gameObject.tag == "Player")
		{
			hit.collider.gameObject.GetComponent<PlayerReborn>().savePos = trans.position + voffset;
		}
	}
}