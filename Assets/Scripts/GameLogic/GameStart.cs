﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {
	private Button btn;

	private void Start()
	{
		btn = GetComponent<Button>();
		btn.onClick.AddListener(gameStart);
	}

	private void gameStart()
	{
		SceneManager.LoadScene("map");
	}
}
