﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class ElementBall : MonoBehaviour
{
    public Element element;
    private Rigidbody2D rigid;
    public Vector2 velocity;
    private Transform trans;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        trans = transform;
    }

    private void Update()
    {
        rigid.velocity += new Vector2(0, element.floatable * Time.deltaTime);
        velocity = rigid.velocity;
    }

    public void AddForce(Vector2 force)
    {
        rigid.AddForce(force, ForceMode2D.Force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        Fire fire;
        if ((fire = collision.gameObject.GetComponent<Fire>()) != null)
        {
            if (element.addfire)
            {
                fire.FireHP++;
                Destroy(gameObject);
            }
            if (element.firable)
            {
                //GameObject go;
                //GameObjectPool pool;
                //if (PoolManager.GetGameObject("CR", out go, out pool))
                //{
                //    ElementBall ball = go.GetComponent<ElementBall>();
                //    ball.transform.position = trans.position;
                //    CentralCoroutine.MyStartCoroutine(Destroy(ball, 5f, pool));
                //}
                //element.firable = false;
                ////gameObject.SetActive(false);
                //gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
            if (element.killfire)
            {
                fire.FireHP-=0.1f;
                Destroy(gameObject);
            }
            if (element.explodable)
            {
                // explosion
                GameObject go;
                GameObjectPool pool;
                if (PoolManager.GetGameObject("Explode", out go, out pool))
                {
                    Explosion explosion = go.GetComponent<Explosion>();
                    explosion.transform.position = trans.position;
                    explosion.Init();
                    explosion.Explode(pool);
                    Destroy(gameObject);
                }
            }
        }
    }

    private IEnumerator Destroy(ElementBall ball, float time, GameObjectPool pool)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        pool.Recycle(ball.gameObject);
        Destroy(gameObject);
    }
}

