﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firable : MonoBehaviour
{
    private Transform trans;
    public FireType type;

    public void Start()
    {
        trans = transform;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Fire fire;
        if ((fire = collision.gameObject.GetComponent<Fire>()) != null)
        {
            GameObject go;
            GameObjectPool pool;
            if (PoolManager.GetGameObject(GetFireName(type), out go, out pool))
            {
                Fire ball = go.GetComponent<Fire>();
                ball.transform.position = trans.position;
            }
            //gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject);
        }
    }

    public string GetFireName(FireType type)
    {
        switch (type)
        {
            case FireType.FireBall:
                return "Fire01";
            case FireType.FireBig:
                return "Fire02";
            case FireType.FireLarge:
                return "Fire03";
        }
        return "Fire01";
    }

    public enum FireType
    {
        FireBall,
        FireBig,
        FireLarge
    }
}
