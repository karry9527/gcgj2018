﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Element : IComparer, IComparable
{
    public string _Symbel;
    public StateType type;
    public bool firable;
    public bool addfire;
    public bool explodable;
    public bool killfire;
    public float floatable;

    public float ShootSpeed;

    public int Compare(object x, object y)
    {
        Element e1 = (Element)x, e2 = (Element)y;
        return e1._Symbel.CompareTo(e2._Symbel);
    }

    public int CompareTo(object obj)
    {
        Element e = (Element)obj;
        return _Symbel.CompareTo(e._Symbel);
    }
}

public enum StateType
{
    Liquid,
    Gas,
    Solid
}
