﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Shootable : MonoBehaviour
{
    public float ShootSpeed = 1;
    private Rigidbody2D rigid;

    private bool immortal = true;
    private GameObjectPool pool;
    private float timer = 0;
    private float LivePeriod = 5f;

    public void Init()
    {
        if(rigid == null)
            rigid = GetComponent<Rigidbody2D>();
            timer = 0;
    }

    public void AddForce(Vector3 force)
    {   
        rigid.AddForce(force, ForceMode2D.Force);
    }
    
    public void SetDestroy(GameObjectPool pool, float live)
    {
        immortal = false;
        LivePeriod = live;
        this.pool = pool;
    }

    private void Update()
    {
        if (!immortal)
        {
            if(timer >= LivePeriod)
            {
                if(pool != null)
                {
                    pool.Recycle(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
            timer += Time.deltaTime;
        }
    }
}
