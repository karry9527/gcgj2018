﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementKeyIn : MonoBehaviour {
    public string keys;
    public bool keying;
    public TextList ListUI;

    public void Start()
    {
        keys = "";
        keying = false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            keys += "C";
            keys = KeySort(keys);
            if (keys.Contains("R"))
            {
                RemoveR();
                ListUI.SetString(keys);
                keys += "R";
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            keys += "H";
            keys = KeySort(keys);
            if (keys.Contains("R"))
            {
                RemoveR();
                ListUI.SetString(keys);
                keys += "R";
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            keys += "O";
            keys = KeySort(keys);
            if (keys.Contains("R"))
            {
                RemoveR();
                ListUI.SetString(keys);
                keys += "R";
            }
        }
        if (Input.GetMouseButton(1))
        {
            if (!keys.Contains("R")) 
                keys += "R";
            else
            {
                RemoveR();
                keys += "R";
            }
        }
        else
        {
            if (keys.Contains("R"))
            {
                RemoveR();
            }
            ListUI.SetString(keys);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            keys = "";
            ListUI.Clear();
        }
    }

    public void Clear()
    {
        keys = "";
    }

    private void RemoveR()
    {

        string[] strs = keys.Split('R');
        keys = "";
        for (int i = 0; i < strs.Length; i++)
        {
            keys += strs[i];
        }

    }

    private string KeySort(string command)
    {
        char[] keys = new char[command.Length];
        for(int i = 0; i < keys.Length; i++)
        {
            keys[i] = command[i];
        }
        Array.Sort(keys);
        return new string(keys);
    }
    
}
