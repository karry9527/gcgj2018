﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public const float startFire = 3;
    public bool immortal = false;

    [SerializeField]
    private float fireHP;
    public float FireHP
    {
        get
        {
            return fireHP;
        }
        set
        {
            fireHP = value;
            if(fireHP <= 0)
                Destroy (gameObject);
            if(fireHP / startFire > 0.5f)
                trans.localScale = Vector3.one * fireHP / startFire;
        }
    }
    private Transform trans;

    private void Start ()
    {
        fireHP = startFire;
        trans = transform;

    }

    private void Update ()
    {
        if(!immortal)
        {
            FireHP -= Time.deltaTime;
        }
    }

}