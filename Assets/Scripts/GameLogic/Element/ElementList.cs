﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementList : MonoBehaviour
{
    public Element _None;
    public Element _Carbon;
    public Element _Hygragon;
    public Element _Oxygen;
    public Element _Water;
    public Element _CO2;
    public Element _Explode;
    public Element _Fireball;
    public Element _FireLine;

    public GameObject _SolidPrefab;
    public GameObject _LiquidPrefab;
    public GameObject _GasPrefab;
    public GameObject _FireBallPrefab;
    public GameObject _FireLinePrefab;
    public GameObject _ExplodePrefab;

    private GameObjectPool _SolidPool;
    private GameObjectPool _LiquidPool;
    private GameObjectPool _GasPool;
    private GameObjectPool _FireBallPool;

    public Transform storage;

    public void Start()
    {
        _SolidPool = new GameObjectPool(_SolidPrefab, 30, storage, storage);
        _LiquidPool = new GameObjectPool(_LiquidPrefab, 30, storage, storage);
        _GasPool = new GameObjectPool(_GasPrefab, 30, storage, storage);
        _FireBallPool = new GameObjectPool(_FireBallPrefab, 30, storage, storage);
    }

    

    public bool GetElement(string keys, bool fire, out Element element, out GameObject go)
    {
        if(keys .Equals( "C"))
        {
            if (fire)
            {
                element = _Fireball;
                go = _FireBallPool.Get();
                return true;
            }
            element = _Carbon;
            go = _SolidPool.Get();
            return true;
        }
        else if (keys.Equals("H"))
        {
            if (fire)
            {
                element = _Explode;
                go = _FireBallPool.Get();
                return true;
            }
            element = _Hygragon;
            go = _GasPool.Get();
            return true;

        }else if (keys.Equals("O"))
        {
            if (fire)
            {
                element = _FireLine;
                go = _FireBallPool.Get();
                return true;
            }
            element = _Oxygen;
            go = _GasPool.Get();
            return true;
        }
        element = _None;
        go = _SolidPool.Get();
        return false;
    }

    public void Recycle(ElementBall ball)
    {
        if(ball.element.Equals(_Hygragon) || ball.element.Equals(_Oxygen) || ball.element.Equals(_CO2))
        {
            _GasPool.Recycle(ball.gameObject);
        }
        if (ball.element.Equals(_Fireball) || ball.element.Equals(_FireLine) || ball.element.Equals(_Explode))
        {
            _FireBallPool.Recycle(ball.gameObject);
        }
        if (ball.element.Equals(_Water))
        {
            _LiquidPool.Recycle(ball.gameObject);
        }
        if (ball.element.Equals(_Carbon) || ball.element.Equals(_None))
        {
            _SolidPool.Recycle(ball.gameObject);
        }
    }

}


public class ElementGroup
{
    public Dictionary<Element, int> elementCount;

    

    public ElementGroup()
    {
        elementCount = new Dictionary<Element, int>();
    }

    public void AddElement(Element element)
    {
        elementCount[element]++;
    }

    public void AddElements(Element[] elements)
    {
        for(int i = 0; i < elements.Length; i++)
        {
            elementCount[elements[i]]++;
        }
    }

    public void Clear()
    {
        foreach (var key in elementCount.Keys)
        {
            elementCount[key] = 0;
        }
    }
}

[System.Serializable]
public enum BaseElement 
{
    _Carbon,
    _Hydragon,
    _Oxygen
}
