﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Explosion : MonoBehaviour
{
    private CircleCollider2D col;
    public float range = 3f;
    public float time = 0.1f;

    public void Init()
    {
        col = GetComponent<CircleCollider2D> ();
        col.radius = 0;
    }

    public void Explode(GameObjectPool pool)
    {
        StartCoroutine(explode(pool));
    }

    private IEnumerator explode(GameObjectPool pool)
    {
        float timer = 0;
        while(timer < time)
        {
            timer += Time.deltaTime;
            col.radius = range * (timer / time);
            yield return null;
        }
        pool.Recycle(gameObject);
    }

}
