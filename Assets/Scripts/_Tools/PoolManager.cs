﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;
    public static PoolManager instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<PoolManager>();
                if(_instance == null)
                {
                    GameObject go = new GameObject();
                    _instance = go.AddComponent<PoolManager>();
                }
            }
            return _instance;
        }
    }

    private Dictionary<string, GameObjectPool> pools;
    public Transform storage;

    public string[] command;
    public GameObject[] prefab;

    private void Awake()
    {
        pools = new Dictionary<string, GameObjectPool>();
        for(int i = 0; i < command.Length; i++)
        {
            pools.Add(command[i], new GameObjectPool(prefab[i], 30, storage, storage));
        }
    }

    public static GameObjectPool CreatePool(string name, GameObject prefab, Transform usableTrans, Transform usingTrans, int count = 10)
    {
        GameObjectPool pool = new GameObjectPool(prefab, count, usableTrans, usingTrans);
        instance.pools.Add(name, pool);
        return pool;
    }
    
    public static bool GetGameObject(string command, out GameObject go, out GameObjectPool pool)
    {   
        if (instance.pools.TryGetValue(command, out pool))
        {
            go = pool.Get();
            return true;
        }
        else
        {
            go = null;
        }
        return false;
    }
}
