﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameObjectPool
{
    public GameObject Prefab;
    private List<GameObject> usingPool;
    private Queue<GameObject> usablePool;
    public Transform usableTrans;
    public Transform usingTrans;

    public GameObjectPool(GameObject prefab, int count, Transform usableTrans, Transform usingTrans)
    {
        this.Prefab = prefab;
        usingPool = new List<GameObject>();
        usablePool = new Queue<GameObject>();
        this.usableTrans = usableTrans;
        this.usingTrans = usingTrans;
        Create(count);
    }

    public void Create(int count)
    {
        GameObject created;
        for(int i = 0; i < count; i++)
        {
            created = GameObject.Instantiate(Prefab);
            usablePool.Enqueue(created);
            created.transform.SetParent(usableTrans);
            created.SetActive(false);
        }
    }

    public GameObject Get()
    {
        if (usablePool.Count <= 0)
        { 
            Create(16);
        }
        GameObject go = usablePool.Dequeue();
        go.transform.SetParent(usingTrans);
        go.SetActive(true);
        return go;
    }

    public T Get<T>() where T : Object
    {
        if (usablePool.Count <= 0)
        {
            Create(16);
        }
        GameObject go = usablePool.Dequeue();
        go.transform.SetParent(usingTrans);
        go.SetActive(true);
        return go.GetComponent<T>();
    }

    public void Recycle(GameObject go)
    {
        if (usingPool.Contains(go))
        {
            usingPool.Remove(go);
        }
        usablePool.Enqueue(go);
        go.transform.SetParent(usableTrans);
        go.SetActive(false);
    }
}
