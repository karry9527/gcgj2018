﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralCoroutine : MonoBehaviour
{
    private static CentralCoroutine _instance;
    public static CentralCoroutine instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CentralCoroutine>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    _instance = go.AddComponent<CentralCoroutine>();
                    instance.coroutines = new List<IEnumerator>();
                    instance.remove = new List<IEnumerator>();
                }
            }
            return _instance;
        }
    }


    private List<IEnumerator> coroutines;
    private List<IEnumerator> remove;
    
	
	// Update is called once per frame
	void Update ()
    {   
		for(int i = 0; i < coroutines.Count; i++)
        {
            if (!coroutines[i].MoveNext())
            {
                remove.Add(coroutines[i]);
            }
        }
        coroutines.RemoveAll(x => remove.Contains(x));
	}

    public static void MyStartCoroutine(IEnumerator coroutine)
    {
        if (!instance.coroutines.Contains(coroutine))
        {
            instance.coroutines.Add(coroutine);
        }
    }
}
