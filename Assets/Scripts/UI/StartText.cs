﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartText : MonoBehaviour
{
	public float step;

	private float delta;
	private Text text;
	private Color tempColor;

	// Use this for initialization
	void Start ()
	{
		text = GetComponent<Text> ();
		delta = 1 / step;
		StartCoroutine(fade());
	}

	IEnumerator fade ()
	{
		while(true)
		{
			for(int i=0 ; i<step ; i++)
			{
				tempColor = text.color;
				tempColor.a -= delta;
				text.color = tempColor;
				yield return null;
			}

			for(int i=0 ; i<step ; i++)
			{
				tempColor = text.color;
				tempColor.a += delta;
				text.color = tempColor;
				yield return null;
			}
		}
	}
}