﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextList : MonoBehaviour {
    private GameObjectPool pool;
    public GameObject prefab;
    public Transform storage;

    public List<TextBlock> currentList;

	// Use this for initialization
	void Start () {
        pool = new GameObjectPool(prefab, 5, storage, storage);
	}

    public void SetString(string command)
    {
        for (int i = 0; i < currentList.Count; i++)
        {
            pool.Recycle(currentList[i].gameObject);
        }
        currentList.Clear();

        Vector3 position = Vector3.zero;
        string last = "";
        int count = 1;
        for (int i = 0; i < command.Length; i++)
        {
            TextBlock block = pool.Get<TextBlock>();
            //if (command[i].Equals(last))
            //{
            //    count++;
            //    block.trans.sizeDelta = TextBlock.SmallSize;
            //    block.trans.localPosition = position;
            //    position.x += TextBlock.SingleSize.x;
            //    block.SetString(count.ToString());
            //    continue;
            //}
            
            
            block.SetString(command[i].ToString());
            block.trans.localPosition = position;
            int v;
            if (int.TryParse(command[i].ToString(), out v))
            {
                block.trans.sizeDelta = TextBlock.SmallSize;
                block.trans.localPosition = position;
                position.x += TextBlock.SingleSize.x;
            }
            else
            { 
                
                block.trans.sizeDelta = TextBlock.SingleSize;
                block.trans.localPosition = position;
                position.x += TextBlock.SingleSize.x;
            }
            currentList.Add(block);
        }

    }

    public void Clear()
    {
        for (int i = 0; i < currentList.Count; i++)
        {
            pool.Recycle(currentList[i].gameObject);
        }
        currentList.Clear();
    }
}
