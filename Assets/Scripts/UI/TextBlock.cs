﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class TextBlock : MonoBehaviour
{
    public readonly static Vector2 DoubleSize = new Vector2(50, 50);
    public readonly static Vector2 SingleSize = new Vector2(32, 50);
    public readonly static Vector2 SmallSize = new Vector2(12.5f, 30);

    public string _String;
    public TextBlockType type;
    public RectTransform trans { get; private set; }
    private Text text;

    private void Awake()
    {
        trans = GetComponent<RectTransform>();
        text = GetComponent<Text>();
    }

    public void SetString(string str)
    {
        int v;
        if (int.TryParse(str, out v))
        {
            type = TextBlockType.Small;
            trans.sizeDelta = SmallSize;
        }
        else if (str.Length == 1)
        {
            type = TextBlockType.Single;
            trans.sizeDelta = SingleSize;
        }
        else if(str.Length == 2)
        {
            type = TextBlockType.Double;
            trans.sizeDelta = DoubleSize;
        }
        text.text = str;
    }
}

public enum TextBlockType
{
    Double,
    Single,
    Small
}
